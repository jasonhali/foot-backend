const mongoose = require('mongoose')
const Schema = mongoose.Schema

const items = new Schema({
  itemName: {
    type: String,
    required: true
  },
  sku: {
    type: String,
    required: true
  },
  retailPrice: {
    type: String,
    required: true
  },
  itemCondition: {
    type: String,
    required: true
  },
  colorway: {
    type: String,
    required: true
  },
  itemRating: {
    type: Number,
    required: true
  },
  gender: {
    type: String,
    required: true
  },
  itemSize: {
    type: Array,
    required: true
  },
  itemType: {
    type: String,
    required: true
  },
  brand: {
    type: String,
    required: true
  },
  releaseDate: {
    type: String,
    required: true
  },
  itemImage: {
    type: Array,
    required: true
  },
  itemDescription: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model('Items', items)
