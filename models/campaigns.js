const mongoose = require('mongoose')
const Schema = mongoose.Schema

// name :''
// title: ''
// start_date:''
// end_date:''
// component_data: {}

const campaigns = new Schema({
  campaignName: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  urlKey: {
    type: String,
    required: true
  },
  startDate: {
    type: String,
    required: true
  },
  endDate: {
    type: String,
    required: true
  },
  campaignData: {
    type: Array,
    required: true
  }
})

module.exports = mongoose.model('Campaigns', campaigns)
