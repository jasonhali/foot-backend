const mongoose = require('mongoose')
const Schema = mongoose.Schema

const campaignsEditor = new Schema({
  data: {
    type: Object,
    required: true
  }
})

module.exports = mongoose.model('CampaignEditor', campaignsEditor)
