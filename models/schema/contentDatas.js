const mongoose = require('mongoose')
const Schema = mongoose.Schema

const contentDatas = new Schema({
  data: {
    type: Object,
    required: true
  }
})

module.exports = mongoose.model('contentDatas', contentDatas)
