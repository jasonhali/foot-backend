const mongoose = require('mongoose')
const Schema = mongoose.Schema

const contentEditors = new Schema({
  data: {
    type: Object,
    required: true
  }
})

module.exports = mongoose.model('ContentEditors', contentEditors)
