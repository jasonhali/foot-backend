const mongoose = require('mongoose')
const Schema = mongoose.Schema

const campaignsEditorTools = new Schema({
  data: {
    type: Object,
    required: true
  }
})

module.exports = mongoose.model('CampaignEditorTools', campaignsEditorTools)
