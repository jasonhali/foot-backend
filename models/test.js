import Person from './person.js'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const test = new Schema({
  person: {
    type: Array,
    required: true
  }
})

module.exports = mongoose.model('Test', test)
