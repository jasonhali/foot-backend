const express = require('express')
const app = express()
const http = require('http')
require('dotenv').config()

const jwt = require('jsonwebtoken')

app.post('/login', (req, res) => {
  // auth user
  const userName = req.body.userName
  const user = { name: userName }
  const accessToken = generateAccessToken(user)
  const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)

  res.json({ accessToken: accessToken, refreshToken: refreshToken })
})

function generateAccessToken (user) {
  // auth user
  return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '15s ' })
}

const server = http.createServer(app)

const PORT = process.env.PORT || 3001

server.listen(PORT)
